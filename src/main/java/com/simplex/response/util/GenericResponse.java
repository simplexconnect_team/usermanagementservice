package com.simplex.response.util;

import java.util.List;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericResponse {

	private String message;
	private String error;
	private int statusCode;
	private Object data;
	private List<Object> dataList;

	public GenericResponse(List<Object> dataList, int statusCode) {
		super();
		this.dataList = dataList;
		this.statusCode = statusCode;
	}

	public GenericResponse(String message, int statusCode) {
		super();
		this.message = message;
		this.statusCode = statusCode;
	}

	public GenericResponse(Object data, int statusCode) {
		super();
		this.data = data;
		this.statusCode = statusCode;
	}

	public GenericResponse(String message, Object data, int statusCode) {
		super();
		this.message = message;
		this.data = data;
		this.statusCode = statusCode;
	}

	public GenericResponse(String message, List<Object> dataList, int statusCode) {
		super();
		this.message = message;
		this.dataList = dataList;
		this.statusCode = statusCode;
	}

	public GenericResponse(String message, String error, int statusCode) {
		super();
		this.message = message;
		this.error = error;
		this.statusCode = statusCode;
	}

	public GenericResponse(List<FieldError> fieldErrors, List<ObjectError> globalErrors) {
		super();
		final ObjectMapper mapper = new ObjectMapper();
		try {
			this.message = mapper.writeValueAsString(fieldErrors);
			this.error = mapper.writeValueAsString(globalErrors);
		} catch (final JsonProcessingException e) {
			this.message = "";
			this.error = "";
		}
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public List<Object> getDataList() {
		return dataList;
	}

	public void setDataList(List<Object> dataList) {
		this.dataList = dataList;
	}

}
