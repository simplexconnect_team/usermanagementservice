package com.simplex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class UsermanagementApplication {
	public static void main(String[] args) {
		SpringApplication.run(UsermanagementApplication.class, args);
	}
}