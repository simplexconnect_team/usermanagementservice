package com.simplex.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "privilege")
public class Privilege{
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "privilegeId")
	private Long id;
    private String privilege;
    @OneToOne(targetEntity=PrivilegeType.class, fetch=FetchType.EAGER)
	@JoinColumn(name = "privilegeTypeId")
	private PrivilegeType privilegeType;

    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getPrivilege() {
        return privilege;
    }
    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }
    
    public PrivilegeType getPrivilegeType() {
		return privilegeType;
	}
	public void setPrivilegeType(PrivilegeType privilegeType) {
		this.privilegeType = privilegeType;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Privilege other = (Privilege) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Privilege [id=" + id + ", privilege=" + privilege + "]";
	}
	
	
    
}