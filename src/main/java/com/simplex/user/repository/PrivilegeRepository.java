package com.simplex.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simplex.user.model.Privilege;

public interface PrivilegeRepository extends JpaRepository<Privilege,Long> {
	
		


}
