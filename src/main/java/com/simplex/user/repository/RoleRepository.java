package com.simplex.user.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.simplex.user.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

   List<Role> findAll();
   
   @SuppressWarnings("unchecked")
   Role save(Role role);
	
}
