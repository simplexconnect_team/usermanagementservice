package com.simplex.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simplex.user.model.PrivilegeType;

public interface PrivilegeTypeRepository extends JpaRepository<PrivilegeType, Long> {
	
	
}
