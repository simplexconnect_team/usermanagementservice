package com.simplex.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simplex.user.model.UserLevel;

public interface UserLevelRepository extends JpaRepository<UserLevel,Long>{
	
	

}
