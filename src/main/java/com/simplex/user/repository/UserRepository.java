package com.simplex.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simplex.user.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	@SuppressWarnings("unchecked")
	User save(User user);
	
	
}
