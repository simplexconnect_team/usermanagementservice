package com.simplex.user.service;

import java.util.List;

import com.simplex.user.model.Privilege;
import com.simplex.user.model.PrivilegeType;
import com.simplex.user.model.Role;

public interface RolePrivilegeService {
	
	List<Role> getAllRoles();
	
	Role createRole(Role role);
	
	void updateRole(List<Role> roleList);
	
	Privilege createPrivilege(Privilege privilege);
	
	List<Privilege> getAllPrivileges();
	
	PrivilegeType createPrivilegeType(PrivilegeType privilegeType);
	
	List<PrivilegeType> getAllPrivilegeTypes();

}
