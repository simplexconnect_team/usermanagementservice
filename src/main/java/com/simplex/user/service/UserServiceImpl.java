package com.simplex.user.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simplex.user.model.User;
import com.simplex.user.repository.UserRepository;

@Service("userService")
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepository;

	@Override
	public User saveUser(User user) {
		User createdUser = userRepository.save(user);
		return createdUser;
	}
	
	

}
