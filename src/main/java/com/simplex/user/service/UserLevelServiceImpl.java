package com.simplex.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simplex.user.model.UserLevel;
import com.simplex.user.repository.UserLevelRepository;

@Service("userLevelService")
public class UserLevelServiceImpl implements UserLevelService {
	
	@Autowired
	UserLevelRepository userLevelRepository;

	@Override
	public List<UserLevel> getAllUserLevels() {
		return userLevelRepository.findAll();
	}

	
}
