package com.simplex.user.service;

import java.util.List;

import com.simplex.user.model.UserLevel;

public interface UserLevelService {
	
	List<UserLevel> getAllUserLevels();

}
