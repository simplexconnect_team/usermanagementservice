package com.simplex.user.service;
import com.simplex.user.model.User;

public interface UserService {

	User saveUser(User user);
	
}
