package com.simplex.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simplex.user.model.Privilege;
import com.simplex.user.model.PrivilegeType;
import com.simplex.user.model.Role;
import com.simplex.user.repository.PrivilegeRepository;
import com.simplex.user.repository.PrivilegeTypeRepository;
import com.simplex.user.repository.RoleRepository;

@Service("RolePrivilegeService")
public class RolePrivilegeServiceImpl implements RolePrivilegeService {

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	PrivilegeRepository privilegeRepository;
	
	@Autowired
	PrivilegeTypeRepository privilegeTypeRepository;
	
	
	@Override
	public List<Role> getAllRoles() {
		return roleRepository.findAll();
	}

	@Override
	public Role createRole(Role role) {		
		return roleRepository.save(role);
	}

	@Override
	public Privilege createPrivilege(Privilege privilege) {
		return  privilegeRepository.save(privilege) ;
	}

	@Override
	public List<Privilege> getAllPrivileges() {
		return privilegeRepository.findAll();
	}

	@Override
	public PrivilegeType createPrivilegeType(PrivilegeType privilegeType) {
		return privilegeTypeRepository.save(privilegeType);
	}

	@Override
	public List<PrivilegeType> getAllPrivilegeTypes() {
		return privilegeTypeRepository.findAll();
	}

	@Override
	public void updateRole(List<Role> roleList) {
		roleList.forEach(role->{
			System.out.println("Role: "+role.toString());
			roleRepository.save(role);
		});	
	}
	
	
	
	

}
