package com.simplex.user.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simplex.response.util.GenericResponse;
import com.simplex.user.service.UserLevelService;

@RestController
@RequestMapping("/users/levels")
public class UserLevelController {
	
	@Autowired
	UserLevelService userLevelService;
	
	@GetMapping
	public ResponseEntity<?> getAllLevels(){
		GenericResponse response = new GenericResponse(userLevelService.getAllUserLevels(), HttpStatus.OK.value());
		return new ResponseEntity<GenericResponse>(response,HttpStatus.OK);
	}
}
