package com.simplex.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simplex.response.util.GenericResponse;
import com.simplex.user.error.ResourceAlreadyExistsException;
import com.simplex.user.model.Role;
import com.simplex.user.service.RolePrivilegeService;

@RestController
@RequestMapping("/roles")
public class RoleController {

	@Autowired
	RolePrivilegeService rolePrivilegeService;
	
	@GetMapping
	public ResponseEntity<?> findAllRoles(){
		GenericResponse response = new GenericResponse(rolePrivilegeService.getAllRoles(),HttpStatus.OK.value());
		return new ResponseEntity<GenericResponse>(response,HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<?> addRole(@RequestBody Role role){
		GenericResponse response;
		try {
			response = new GenericResponse(rolePrivilegeService.createRole(role),HttpStatus.CREATED.value());
		} catch (DataIntegrityViolationException e) {
			throw new ResourceAlreadyExistsException("Role already exists");
		}	
		return new ResponseEntity<GenericResponse>(response,HttpStatus.OK);
	}
	
	
	@PostMapping("/privileges")
	public ResponseEntity<?> assignPrivileges(@RequestBody List<Role> roleList){
		System.out.println("Assign Privileges");
		try {
			rolePrivilegeService.updateRole(roleList);
		} catch (Exception e) {
			e.printStackTrace();
		}
			return new ResponseEntity<GenericResponse>(new GenericResponse("Successfully Assign Privileges", HttpStatus.CREATED.value()),HttpStatus.CREATED);
		
	}
	
	
	
}
