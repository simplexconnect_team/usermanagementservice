package com.simplex.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simplex.response.util.GenericResponse;
import com.simplex.user.error.ResourceAlreadyExistsException;
import com.simplex.user.model.Privilege;
import com.simplex.user.model.PrivilegeType;
import com.simplex.user.service.RolePrivilegeService;

@RestController
@RequestMapping("/privileges")
public class PrivilegeController {
	
	@Autowired
	RolePrivilegeService rolePrivilegeService;

	@PostMapping
	public ResponseEntity<?> createPrivilege(@RequestBody Privilege privilege){
		GenericResponse response;
		
		try {
			response = new GenericResponse("Successfully create privilege",rolePrivilegeService.createPrivilege(privilege),HttpStatus.OK.value());
		} catch (DataIntegrityViolationException e) {
			throw new ResourceAlreadyExistsException("Privilege already exists");
		}	
		return new ResponseEntity<GenericResponse>(response,HttpStatus.OK);	
	}
	
	@GetMapping
	public ResponseEntity<?> getAllPrivileges(){
		GenericResponse response = new GenericResponse(rolePrivilegeService.getAllPrivileges(),HttpStatus.OK.value());
		return new ResponseEntity<GenericResponse>(response,HttpStatus.OK);	
	}
	
	
	@PostMapping("/types")
	public ResponseEntity<?> createPrivilegeType(@RequestBody PrivilegeType privilegeType){
		GenericResponse response;
		try {
			response = new GenericResponse("Successfully create privilege type",rolePrivilegeService.createPrivilegeType(privilegeType),HttpStatus.OK.value());
		} catch (DataIntegrityViolationException e) {
			throw new ResourceAlreadyExistsException("Privilege type already exists");
		}	
		return new ResponseEntity<GenericResponse>(response,HttpStatus.OK);	
	}
	
	@GetMapping("/types")
	public ResponseEntity<?> getAllPrivilegeTypes(){
		GenericResponse response = new GenericResponse(rolePrivilegeService.getAllPrivilegeTypes(),HttpStatus.OK.value());
		return new ResponseEntity<GenericResponse>(response,HttpStatus.OK);	
	}
	
}
