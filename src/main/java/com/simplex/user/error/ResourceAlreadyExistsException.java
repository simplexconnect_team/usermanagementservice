package com.simplex.user.error;

public final class ResourceAlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = 1455728158296633232L;

	public ResourceAlreadyExistsException(final String message){
		super(message);
	}
}
