# User Management Service API Documentation

Sample JWT Body
--javascript
{
  "privileges": {
    "Order": [
      "Edit_or_Delete_Orders",
      "Receive_Completed_Orders",
      "View_All_Orders",
      "Receive_Return_Orders",
      "Add_Orders",
      "View_Order_Menu"
    ],
    "Utility": [
      "Quick_Order_Search",
      "Add_Daily_Client_Pickup_Request",
      "View_Rate_Card_Calculator",
      "View_Daily_Client_Pickup_Requests"
    ],
    "System_Support": [
      "View_Client_Issues"
    ],
    "Report": [
      "Age_Report",
      "Order_Report",
      "Delivery_Report",
      "View_Report_Menu",
      "Processing_Orders_Report"
    ],
    "Billing": [
      "View_Billing_Menu",
      "View_Weight_and_Category",
      "View_Invoices",
      "View_Supplier_Pickup_Invoices",
      "View_Client_Invoices"
    ],
    "Supplier_Pickup": [
      "Add_Supplier_Pickup",
      "View_Supplier_Pickup_Menu",
      "Convert_Supplier_Pickup_to_Delivery"
    ],
    "COD": [
      "View_COD_Payments",
      "View_COD_Menu",
      "COD_Summary"
    ],
    "Settings": [
      "View_Settings_Menu",
      "Email_Configuration"
    ]
  },
  "sub": "testclient1",
  "role": "Client",
  "iss": "https://simplexdelivery.com",
  "exp": 1538735896,
  "iat": 1538649496
}
```

##Post  /auth/login  

### Request

--javascript
{
    "username": "string",
    "password": "string"
}
```

### Success Response

### 200 OK

--javascript
{
    "token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}
```

### Error Responce

### 401 Unauthorized

--javascript
{
    "error": "Unauthorized",
	"message": "Invalid username or password",
 	"statusCode": 401
}
```

### 415 UnsupportedContentType

--javascript
{
    "error": "UnsupportedContentType",
	"message": "Unsupported content type:{provided content type}, Supported content types:application/json",
 	"statusCode": 415
}
```

### 400 Bad Request

--javascript
{
    "error": "Bad Request",
	"message": "Invalid json format",
 	"statusCode": 400
}
```

##Get  /users/levels 

###Success Response

### 200 OK 

--javascript
{
     "statusCode": 200,
    "object": [
        {
            "id": 1,
            "level": "Top Level",
            "roles":[{
						"id":1,
						"role":"site-admin"
					 },
					 {
						"id":2,
						"role":"super-user"
				  	}]
        },
        {
            "id": 2,
            "level": "Organizational Level",
            "roles":[]
        }
    ]  
}
```

##Post  /users/levels 

###Request 

--javascript
{
	"level":"Top Level"
}

###Success Response

### 200 ok 

--javascript
{	
	"message":"user level created",
    "statusCode": 200,
    "object":
        {
            "id": 1,
            "level": "Top Level"
        }
     
}

###Error Response

### 409 conflict 

--javascript
{
	"message": "User level already exists",
    "error": "Conflict",
 	"statusCode": 401
}

	
## get  /roles

###success response

###200 OK response

--javascript
{
    "statusCode": 200,
    "object": [
        {
            "id": 1,
            "role": "Site Admin",
            "privileges": []
        },
        {
            "id": 2,
            "role": "Super User",
            "privileges": []
        }
     ]
}


## Post  /roles
-- 
###Request

--javascript
{
	"role":"site-admin"
	
}

###success response

### 200 OK Status

--javascript
  {
  	"message": "Role Created",
    "statusCode": 200,
    "object": 
        {
         "id":1,
         "role":"site-admin"
      }
  }
  
###Error Response

### 409 conflict 

--javascript
{
    "message": "Role already exists",
    "error": "Conflict",
    "statusCode": 409
}
  
## Post  /users/levels/{id}/roles

###Request

--javascript
{
	"id":1,
	"roles":[
		{
		"id":1,
		"role":"site-admin"
		},
		{
		"id":2,
		"role":"super-user"
		}
	]
	
}


###success response

### 200 OK Status

--javascript
  {
  	"message": "Roles Assign to User Level",
    "statusCode": 200,
    "object": 
        {
	 "id":1,
	 "level": "Top Level",
	 "roles":[
		{
		"id":1,
		"role":"site-admin"
		},
		{
		"id":2,
		"role":"super-user"
		}
	]
	
  }
  
  
## Get  /privileges

### 200 OK Status

--javascript
  {
    "statusCode": 200,
    "object": 
       [{
		 "id":1,
		 "privilege": "Create Users"
	    },
	    {
		 "id":1,
		 "privilege": "View Orders"
	    }
	   ]
  }
  
## Post  /privileges

### request

--javascript
{
	"privilege":"Add Orders"
}

### success response

###200 OK Status

--javascript
  {
    "message":"Privilege created successfully"
    "statusCode": 200,
    "object": 
       {
		 "id":1,
		 "privilege": "Add Orders"
	   }
  }

###Error Response

### 409 conflict 

--javascript
{
    "message": "Privilege already exists",
    "error": "Conflict",
 	"statusCode": 401
}

## Post  /users/levels/{id}/roles/{id}/privileges

###Request

--javascript
{
	"userLevel":{
				"id":1,
				"level":"top-level"
				},
	"role":{
		   "id":1,
		   "role":"site-admin"
		   },
	"privileges":[
		{
		"id":1,
		"privilege":"create users"
		},
		{
		"id":2,
		"privilege":"view orders"
		}
	]
	
}


###success response

### 200 OK Status

--javascript
  {
  	"message": "Privileges Assign to Roles",
    "statusCode": 200,
    "object": 
        {
	 "id":1,
	 "role": "Site Admin",
	 "privileges":[
		{
		"id":1,
		"privilege":"create users"
		},
		{
		"id":2,
		"privilege":"view orders"
		}
	]
  }
  
  
## Get  /users

###success response

### 200 OK Status

--javascript
[{
	"id":1,
	"name":"majith robert",
	"userName":"majithg",
	"email":"majithg@gmail.com",
	"phone":"0718119110",
	"userLevel":   {
                   "id": 1,
                   "level": "Top Level",
                   "roles":[{
						"id":1,
						"role":"site-admin"
					 },
					 {
						"id":2,
						"role":"super-user"
				  	}]
                    }
	"role": {
            "id": 1,
            "role": "Site Admin",
            "privileges": []
            },
    enabled:true,
    active:true,
	"createdBy":"Nifras",
	"createdAt":"2018-09-02 12:10:13",
	"updatedBy":"anuthiran",
	"changedAt":"2018-09-04 14:12:12",
	"lastLoginAt":"2018-09-08 08:10:15"
	
},
{
	"id":2,
	"name":"anuthiran kovintharaja",
	"userName":"anuthirank",
	"email":"anuk@gmail.com",
	"phone":"0718119110",
	"userLevel":   {
                   "id": 1,
                   "level": "Top Level",
                   "roles":[{
						"id":1,
						"role":"site-admin"
					 },
					 {
						"id":2,
						"role":"super-user"
				  	}]
                    }
	"role": {
            "id": 1,
            "role": "Site Admin",
            "privileges": []
            },
    enabled:true,
    active:true,
	"createdBy":"Nifras",
	"createdAt":"2018-09-02 12:10:13",
	"updatedBy":"anuthiranK",
	"changedAt":"2018-09-04 14:12:12",
	"lastLoginAt":"2018-09-08 08:10:15"
	------
}
]

## Post  /users

###Request

 ---javascript
 {
    "name": "majith robert",
	"userName": "majithg",
	"email": "majithg@gmail.com",
	"password": "majith123",
	"phone": "0718119110",
 	"userLevel": {
 				 "id": 1,
                 "level": "Top Level",
 				 },
 	"role": {
            "id": 1,
            "role": "Site Admin",
            } 
    enabled: true,
}  


###success response

### 200 OK Status

--javascript
  {
  	"message": "User Created",
    "statusCode": 200,
    "object": 
        {
        "id":1,
	    "name":"majith robert",
	    "userName":"majithg",
      }
	]
  }
  
  
###Error Response

### 409 conflict 

--javascript
{
    "message": "User Name already exists",
    "error": "Conflict",
 	"statusCode": 401
}




## Patch  /users/{id}

###Request

 ---javascript
 {
 	"id":1
	"email": "majithg2@gmail.com",
	"phone": "0718118110",
 	"userLevel": {
 				 "id": 1,
                 "level": "Top Level",
 				 },
 	"role": {
            "id": 1,
            "role": "Site Admin",
            } 
    enabled: true
} 


###success response

### 200 OK Status

--javascript
  {
  	"message": "User Updated",
    "statusCode": 200,
    "object": 
        {
        "id":1,
	    "name":"majith robert",
	    "userName":"majithg",
	    --
      }
	]
  }
  

  




  
  
  
  











































